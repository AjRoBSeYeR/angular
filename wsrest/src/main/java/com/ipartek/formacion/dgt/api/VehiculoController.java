package com.ipartek.formacion.dgt.api;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ipartek.formacion.modelo.daos.VehiculoDAO;
import com.ipartek.formacion.modelo.pojo.Vehiculo;

import io.swagger.annotations.Api;

@CrossOrigin
@RestController

@Api(tags = { "VEHICULO" }, produces = "application/json", description="Gestión de Vehiculos")
public class VehiculoController {
	private static VehiculoDAO vehiculoDAO;
	private final static Logger LOG = Logger.getLogger(VehiculoController.class);

	public VehiculoController() {
		super();
		vehiculoDAO = VehiculoDAO.getInstance();
	}

	@RequestMapping(value = { "/api/vehiculo" }, method = RequestMethod.GET)
	public ArrayList<Vehiculo> listar() {
		LOG.trace("listar");
		return vehiculoDAO.getAll();

		
	}

	
	@RequestMapping(value = { "/api/vehiculo/{matricula}" }, method = RequestMethod.GET)
	public ResponseEntity<Vehiculo> getByMatricula(@PathVariable String matricula) {
		LOG.trace("buscar por matricula");
		ResponseEntity<Vehiculo> response = new ResponseEntity<Vehiculo>(HttpStatus.NOT_FOUND);
		try {
			Vehiculo c = vehiculoDAO.getByMatricula(matricula);
			if (c != null) {
				response = new ResponseEntity<Vehiculo>(c, HttpStatus.OK);
			}
		} catch (Exception e) {
			response = new ResponseEntity<Vehiculo>(HttpStatus.NOT_FOUND);
			LOG.error(e);
		}
		return response;
	}


}
