package com.ipartek.formacion.dgt.api;

import java.util.List;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ipartek.formacion.modelo.pojo.Agente;
import com.ipartek.formacion.modelo.pojo.Multa;
import com.ipartek.formacion.service.impl.AgenteServiceImpl;

import io.swagger.annotations.Api;

@CrossOrigin
@RestController

@RequestMapping("/api/agente")

@Api(tags = { "AGENTE" }, produces = "application/json", description = "Gestión de Agentes")
public class AgenteController {

	private AgenteServiceImpl agenteService;
	private final static Logger LOG = Logger.getLogger(AgenteController.class);
	private ValidatorFactory factory;
	@SuppressWarnings("unused")
	private Validator validator;

	public AgenteController() {
		super();
		agenteService = AgenteServiceImpl.getInstance();
		factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public ResponseEntity<List<Agente>> listar() {
		ResponseEntity<List<Agente>> response = new ResponseEntity<List<Agente>>(HttpStatus.NOT_FOUND);
		List<Agente> agentes = agenteService.getAll();
		if (agentes != null) {
			response = new ResponseEntity<List<Agente>>(agentes, HttpStatus.OK);
		}

		return response;

	}

	@RequestMapping(value = { "/{id}/multa/{cualVer}" }, method = RequestMethod.GET)
	public ResponseEntity<List<Multa>> multasagente(@PathVariable String id, @PathVariable String cualVer) {
		Integer id_int = null;
		List<Multa> multas = null;
		ResponseEntity<List<Multa>> response = new ResponseEntity<List<Multa>>(HttpStatus.NOT_FOUND);
		try {
			id_int = Integer.parseInt(id);
			multas = agenteService.obtenerMultas(id_int, cualVer);
			if (multas != null) {
				response = new ResponseEntity<List<Multa>>(multas, HttpStatus.OK);
			}
		} catch (Exception e) {
			LOG.error(e);
			response = new ResponseEntity<List<Multa>>(HttpStatus.BAD_REQUEST);
		}
		return response;
	}

	@RequestMapping(value = { "/login/{placa}/{pass}" }, method = RequestMethod.GET)
	public ResponseEntity<Agente> login(@PathVariable String placa, @PathVariable String pass) {
		Agente agente = null;
		ResponseEntity<Agente> response = new ResponseEntity<Agente>(agente, HttpStatus.NOT_FOUND);
		agente = agenteService.existe(placa, pass);
		if (agente == null) {
			response = new ResponseEntity<Agente>(agente, HttpStatus.FORBIDDEN);
		} else {
			response = new ResponseEntity<Agente>(agente, HttpStatus.OK);
		}
		return response;
	}

	@RequestMapping(value = { "/ponmulta" }, method = RequestMethod.POST)
	public ResponseEntity<Multa> multar(@RequestBody Multa multa ) {
		ResponseEntity<Multa> response = new ResponseEntity<Multa>(multa, HttpStatus.INTERNAL_SERVER_ERROR);
		if (multa == null) {
			response = new ResponseEntity<Multa>(multa, HttpStatus.BAD_REQUEST);
		} else {
			try {
				multa = agenteService.multar(multa.getVehiculo().getId(), multa.getAgente().getId(), multa.getConcepto(),
						multa.getImporte());
				if (multa != null) {
					response = new ResponseEntity<Multa>(multa, HttpStatus.OK);
				}
			} catch (Exception e) {
				response = new ResponseEntity<Multa>(multa, HttpStatus.BAD_REQUEST);
				LOG.error(e);
			}
		}
		return response;
	}
	
	@RequestMapping(value = { "/estadisticas/{anio}" }, method = RequestMethod.POST)
	public ResponseEntity<Multa> estadistcas(@RequestParam String anio ) {
		ResponseEntity<Multa> response = new ResponseEntity<Multa>(HttpStatus.INTERNAL_SERVER_ERROR);
			try {
				int anio_int= Integer.parseInt(anio);
			} catch (Exception e) {
				response = new ResponseEntity<Multa>(HttpStatus.BAD_REQUEST);
				LOG.error(e);
			}
		return response;
	}
	
	@RequestMapping(value = { "/accionmulta/{id}/{accion}" }, method = RequestMethod.PATCH)
	public ResponseEntity<Multa> anular_recuperar_multa(@PathVariable String id, @PathVariable String accion) {
		Integer id_int = null;
		ResponseEntity<Multa> response = new ResponseEntity<Multa>(HttpStatus.NOT_FOUND);
		try {
			id_int = Integer.parseInt(id);
			Boolean accionCorrecta = agenteService.accionMulta(id_int,accion);
			if (accionCorrecta) {
				response = new ResponseEntity<Multa>(HttpStatus.OK);
			}
		} catch (Exception e) {
			LOG.error(e);
			response = new ResponseEntity<Multa>(HttpStatus.BAD_REQUEST);
		}
		return response;
	}

}
