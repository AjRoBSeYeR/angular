import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AutorizacionService } from '../providers/autorizacion.service';

@Injectable({
  providedIn: 'root'
})
export class PermisoGuard implements CanActivate {
  constructor(
    private autorizacionService: AutorizacionService,
    private router: Router) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    console.trace('PermisoGuard canActivate ');
    if (this.autorizacionService.isLoged()) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }

  }

}
