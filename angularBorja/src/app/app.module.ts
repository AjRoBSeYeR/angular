import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { MenuprincipalComponent } from './components/menuprincipal/menuprincipal.component';
import { MultasagenteComponent } from './components/multasagente/multasagente.component';
import { Pagina404Component } from './components/pagina404/pagina404.component';
import { FormmultaComponent } from './components/formmulta/formmulta.component';
import { BuscamatriculaComponent } from './components/buscamatricula/buscamatricula.component';
import { MultasanuladasagenteComponent } from './components/multasanuladasagente/multasanuladasagente.component';
import { InfoagenteComponent } from './components/infoagente/infoagente.component';
import { ObjetivosComponent } from './components/objetivos/objetivos.component';
import { MensajesComponent } from './components/mensajes/mensajes.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuprincipalComponent,
    MultasagenteComponent,
    Pagina404Component,
    FormmultaComponent,
    BuscamatriculaComponent,
    MultasanuladasagenteComponent,
    InfoagenteComponent,
    ObjetivosComponent,
    MensajesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,   
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
