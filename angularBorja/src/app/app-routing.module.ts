import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { MenuprincipalComponent } from './components/menuprincipal/menuprincipal.component';
import { PermisoGuard } from './guards/permiso.guard';
import { MultasagenteComponent } from './components/multasagente/multasagente.component';
import { Pagina404Component } from './components/pagina404/pagina404.component';
import { FormmultaComponent } from './components/formmulta/formmulta.component';
import { BuscamatriculaComponent } from './components/buscamatricula/buscamatricula.component';
import { MultasanuladasagenteComponent } from './components/multasanuladasagente/multasanuladasagente.component';
import { ObjetivosComponent } from './components/objetivos/objetivos.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'menuprincipal', component: MenuprincipalComponent, canActivate:[PermisoGuard] },
  { path: 'multasagente', component: MultasagenteComponent, canActivate:[PermisoGuard] },
  { path: 'multasanuladasagente', component: MultasanuladasagenteComponent, canActivate:[PermisoGuard] },
  { path: 'nuevamulta', component: FormmultaComponent, canActivate:[PermisoGuard] },
  { path: 'buscamatricula', component: BuscamatriculaComponent, canActivate:[PermisoGuard] },
  { path: 'verobjetivos', component: ObjetivosComponent, canActivate:[PermisoGuard] },
  { path: '', component: LoginComponent },
  { path: '404', component: Pagina404Component},
  { path: '**', pathMatch:'full', redirectTo : '404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
