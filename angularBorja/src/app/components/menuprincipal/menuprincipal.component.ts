import { Component, OnInit } from '@angular/core';
import { Mensaje } from 'src/app/model/mensaje';
import { MensajesService } from 'src/app/providers/mensajes.service';

@Component({
  selector: 'app-menuprincipal',
  templateUrl: './menuprincipal.component.html',
  styleUrls: ['./menuprincipal.component.scss']
})

export class MenuprincipalComponent implements OnInit {
  mensaje:Mensaje;

  constructor(
    private mensajesService:MensajesService
  ) {
    console.trace('MenuprincipalComponent constructor');
    this.mensajesService.mensaje=new Mensaje('');
  }

  ngOnInit() {
    console.trace('MenuprincipalComponent ngOnInit');

  }

}
