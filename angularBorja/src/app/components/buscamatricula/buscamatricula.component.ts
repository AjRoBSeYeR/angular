import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { VehiculoService } from 'src/app/providers/vehiculo.service';
import { Router } from '@angular/router';
import { Mensaje } from 'src/app/model/mensaje';

@Component({
  selector: 'app-buscamatricula',
  templateUrl: './buscamatricula.component.html',
  styleUrls: ['./buscamatricula.component.scss']
})
export class BuscamatriculaComponent implements OnInit {
  formulario: FormGroup;
  mensaje: Mensaje;
  constructor(
    private formBuilder: FormBuilder,
    private vehiculoService: VehiculoService,
    private router: Router
  ) {
    this.crearFormulario();
  }

  ngOnInit() {
  }

  crearFormulario() {

    this.formulario = this.formBuilder.group({

      // FormControl nombre
      matricula: [
        '',
        [Validators.required, Validators.minLength(6), Validators.maxLength(10)]
      ],
    })
  }

  buscarmatricula() {
    this.vehiculoService.obtenerVehiculoMatricula(this.formulario.controls[('matricula')].value).subscribe(
      data => {
        this.vehiculoService.pasaVehiculo(data);
        this.router.navigate(['/nuevamulta']);
      },
      error => {
        this.mensaje = new Mensaje('El vehículo buscado no se encuentra', Mensaje.ERROR);
        console.error(error);
      }
    );
  }

}
