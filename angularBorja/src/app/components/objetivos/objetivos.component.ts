import { Component, OnInit } from '@angular/core';
import { ObjetivosService } from 'src/app/providers/objetivos.service';

@Component({
  selector: 'app-objetivos',
  templateUrl: './objetivos.component.html',
  styleUrls: ['./objetivos.component.scss']
})
export class ObjetivosComponent implements OnInit {

  constructor(
    private objetivosService:ObjetivosService,
  ) { 
    
  }

  ngOnInit() {
    this.cargarObjetivos();
  }

  cargarObjetivos(){
    
  }
}
