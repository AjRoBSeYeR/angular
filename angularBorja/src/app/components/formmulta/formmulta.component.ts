import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Vehiculo } from 'src/app/model/vehiculo';
import { VehiculoService } from 'src/app/providers/vehiculo.service';
import { AgenteService } from 'src/app/providers/agente.service';
import { AutorizacionService } from 'src/app/providers/autorizacion.service';
import { Router } from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { Mensaje } from 'src/app/model/mensaje';
import { MensajesService } from 'src/app/providers/mensajes.service';


@Component({
  selector: 'app-formmulta',
  templateUrl: './formmulta.component.html',
  styleUrls: ['./formmulta.component.scss']
})
export class FormmultaComponent implements OnInit {
  formulario: FormGroup;
  vehiculoBuscado: Vehiculo;
  mensaje: Mensaje;
  constructor(
    private formBuilder: FormBuilder,
    private agenteService: AgenteService,
    private mensajesService: MensajesService,
    private router: Router,
    private vehiculoService: VehiculoService
  ) {
    this.crearFormulario();
  }

  ngOnInit() {
  }

  crearFormulario() {
    console.trace('FormmultaComponent crearFormulario')
    this.vehiculoBuscado = this.vehiculoService.getVehiculoBuscado();
    this.formulario = this.formBuilder.group({

      // FormControl nombre
      concepto: [
        '',
        [Validators.required, Validators.minLength(6), Validators.maxLength(10)]
      ],
      importe: [
        '',
        [Validators.required]
      ],
      idvehiculo: [
        '',
        [Validators.required]
      ],
    })
  }

  ponerMulta() {
    console.trace('FormmultaComponent ponerMulta');
    let concepto = this.formulario.controls.concepto.value;
    let importe = this.formulario.controls.importe.value;
    this.agenteService.postMulta(concepto, importe).subscribe(
      data => {
        this.mensaje = new Mensaje('Multa guardada con éxito', Mensaje.OK);
        this.mensajesService.mensaje = this.mensaje;
        this.router.navigate(['/multasagente']);
      },
      error => {
        this.mensaje = new Mensaje('Error al guardar la multa', Mensaje.ERROR);
        console.error(error);
      }
    );

  }

}
