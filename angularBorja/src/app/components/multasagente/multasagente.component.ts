import { Component, OnInit } from '@angular/core';
import { AgenteService } from 'src/app/providers/agente.service';
import { Multa } from 'src/app/model/multa';
import { Router } from '@angular/router';
import { Mensaje } from 'src/app/model/mensaje';
import { MensajesService } from 'src/app/providers/mensajes.service';


@Component({
  selector: 'app-multasagente',
  templateUrl: './multasagente.component.html',
  styleUrls: ['./multasagente.component.scss']
})
export class MultasagenteComponent implements OnInit {
  multas: Multa[];
  storage = window.sessionStorage;
  mensaje:Mensaje;
  constructor(
    private agenteService: AgenteService,
    private mensajesService: MensajesService,
    private router: Router
  ) {
    this.multas = [];
    this.mensaje=this.mensajesService.mensaje;
  }

  ngOnInit() {
    this.cargarMultasAgente();
  }

  cargarMultasAgente() {
    console.trace('MultasagenteComponent cargarMultasAgente');
    let agenteJSON = JSON.parse(this.storage.getItem('agente'));
    this.agenteService.getMultas(agenteJSON._id, "activas").subscribe(
      data => {
        console.debug('datos en json %o', data);
        this.multas = data;
      },
      error => {
        console.error(error);
      }
    );


  }
  anularMulta(id: number) {
    console.trace('MultasagenteComponent anularMulta');
    let confirmado = confirm("¿Esta seguro?");
    if (confirmado) {
      this.agenteService.accionMulta(id,"anular").subscribe(
        data => {
          this.mensaje = new Mensaje('Multa anulada con éxito', Mensaje.OK);
          this.mensajesService.mensaje = this.mensaje;
          this.router.navigate(['/multasanuladasagente']);
          console.debug('datos en json %o', data);
        },
        error => {
          console.error(error);
        }
      );
    }
  }

}
