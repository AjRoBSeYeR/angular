import { Component, OnInit } from '@angular/core';
import { AgenteService } from 'src/app/providers/agente.service';
import { MensajesService } from 'src/app/providers/mensajes.service';
import { Multa } from 'src/app/model/multa';
import { Router } from '@angular/router';
import { Mensaje } from 'src/app/model/mensaje';

@Component({
  selector: 'app-multasanuladasagente',
  templateUrl: './multasanuladasagente.component.html',
  styleUrls: ['./multasanuladasagente.component.scss']
})
export class MultasanuladasagenteComponent implements OnInit {
  multas: Multa[];
  mensaje:Mensaje;
  storage = window.sessionStorage;
  constructor(
    private agenteService: AgenteService,
    private mensajeService:MensajesService,
    private router:Router
  ) {
    this.multas = [];
    this.mensaje=this.mensajeService.mensaje;
  }

  ngOnInit() {
    this.cargarMultasAnuladasAgente();
  }

  cargarMultasAnuladasAgente() {
    console.trace('MultasagenteComponent cargarMultasAgente');
    let agenteJSON = JSON.parse(this.storage.getItem('agente'));
    this.agenteService.getMultas(agenteJSON._id,"anuladas").subscribe(
      data => {
        console.debug('datos en json %o', data);
        this.multas = data;
      },
      error => {
        console.error(error);
      }
    );


  }
  recuperarMulta(id:number){
    console.trace('MultasagenteComponent recuperarMulta');
    this.agenteService.accionMulta(id,"recuperar").subscribe(
      data => {
        this.mensaje = new Mensaje('Multa recuperada con éxito', Mensaje.OK);
        this.mensajeService.mensaje = this.mensaje;
        this.router.navigate(['/multasagente']);
        console.debug('datos en json %o', data);
      },
      error => {
        console.error(error);
      }
    );
  }
}
