import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-infoagente',
  templateUrl: './infoagente.component.html',
  styleUrls: ['./infoagente.component.scss']
})
export class InfoagenteComponent implements OnInit {
  agenteLogueado: any;
  storage = window.sessionStorage;
  constructor(
    private router:Router
  ) {
    this.obtenerLogin();
  }

  obtenerLogin() {
  
    this.agenteLogueado = JSON.parse(this.storage.getItem('agente'));
  }
  ngOnInit() {
  }

  logout(){
this.storage.removeItem('agente');
this.storage.setItem('isLogged','false');
this.router.navigate(['/login']);
  }
}
