import { Component, OnInit, Input } from '@angular/core';
import { Mensaje } from 'src/app/model/mensaje';

@Component({
  selector: 'app-mensajes',
  templateUrl: './mensajes.component.html',
  styleUrls: ['./mensajes.component.scss']
})
export class MensajesComponent implements OnInit {

  @Input() mensaje: Mensaje;

  constructor() { 
    this.mensaje = new Mensaje('');
  }

  ngOnInit() {
  }

}
