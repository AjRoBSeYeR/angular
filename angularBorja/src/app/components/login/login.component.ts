import { Component, OnInit } from '@angular/core';
import { AutorizacionService } from 'src/app/providers/autorizacion.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Agente } from 'src/app/model/agente';
import { environment } from 'src/environments/environment';
import { Mensaje } from 'src/app/model/mensaje';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  formulario: FormGroup;
  texto: string = environment.texto;
  mensaje:Mensaje;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private autorizacionService: AutorizacionService
  ) {
    console.trace('LoginComponent constructor');
    this.crearFormulario();
  }

  ngOnInit() {
    console.trace('LoginComponent ngOnInit');
  }

  crearFormulario() {

    this.formulario = this.formBuilder.group({

      // FormControl nombre
      placa: [
        '',
        [Validators.required, Validators.min(100000), Validators.max(999999)]
      ],
      // end FormControl nombre    
      pass: [
        '',
        [Validators.required, Validators.minLength(3), Validators.maxLength(12)]
      ],
    })
  }

  comprobar() {
    console.trace('click boton submit');
    let placa = this.formulario.controls.placa.value;
    let pass = this.formulario.controls.pass.value;
    console.debug('nombre: %s password: %s', placa, pass);

    //llamar servicio TODO retornar Observable
    this.autorizacionService.login(placa, pass).subscribe(
      data => {
        this.autorizacionService.setLoged(true);
        let agente = new Agente(data.nombre, data.placa, '', data.id);
        this.autorizacionService.saveAgente(agente);
        this.router.navigate(['/menuprincipal']);
      },
      error => {
        this.mensaje=new Mensaje("Error de login, compruebe sus credenciales",Mensaje.ERROR);
        this.autorizacionService.setLoged(false);
        console.error(error);
      }
    )



  }// comprobar
}