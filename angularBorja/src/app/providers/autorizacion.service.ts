import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Agente } from '../model/agente';
import { GLOBAL } from '../global';

@Injectable({
  providedIn: 'root'
})
export class AutorizacionService {
  private storage = window.sessionStorage;

  public isLoged(): boolean {
    if (this.storage.getItem('isLogged') === "true") {
      return true;
    } else {
      return false;
    }
  }
  public setLoged(value: boolean) {
    this.storage.setItem('isLogged', 'true');
  }
  public saveAgente(agente: Agente) {
    this.storage.setItem('agente', JSON.stringify(agente));
  }
  public getAgente(): any {

    let agenteString = this.storage.getItem('agente');
    if (agenteString) {
      return JSON.parse(agenteString);
    } else {
      return undefined;
    }

  }

  endpoint: String;
  constructor(
    private httpClient: HttpClient
  ) {

  }

  login(placa: string, pass: string): Observable<any> {

    let url = GLOBAL.server + 'wsrestBorja/api/agente/login/' + placa + '/' + pass;
    return this.httpClient.get(url);
  }

  logout() {
    this.storage.removeItem('agente');
    this.setLoged(false);
  }
}
