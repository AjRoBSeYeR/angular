import { Injectable } from '@angular/core';
import { Mensaje } from '../model/mensaje';

@Injectable({
  providedIn: 'root'
})
export class MensajesService {
  private _mensaje: Mensaje;
  public get mensaje(): Mensaje {
    return this._mensaje;
  }
  public set mensaje(value: Mensaje) {
    this._mensaje = value;
  }
  constructor(
  ) {

  }

}
