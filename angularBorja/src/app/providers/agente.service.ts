import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Vehiculo } from '../model/vehiculo';
import { VehiculoService } from './vehiculo.service';
import { AutorizacionService } from './autorizacion.service';
import { Agente } from '../model/agente';
import { Multa } from '../model/multa';
import { GLOBAL } from '../global';


@Injectable({
  providedIn: 'root'
})
export class AgenteService {
  private storage = window.sessionStorage;
  endpoint = GLOBAL.server + 'wsrestBorja/api/agente/';

  constructor(
    private vehiculoService: VehiculoService,
    private autorizacionService: AutorizacionService,
    private httpClient: HttpClient) {

    console.trace("AgenteService contructor");
  }

  public getAll(): Observable<any> {
    console.trace('getAll ' + this.endpoint);
    return this.httpClient.get(this.endpoint);
  }

  public getMultas(id: number, cualVer: string): Observable<any> {
    let url = this.endpoint
    url += id + '/multa/' + cualVer;
    console.trace('getMultas ' + url);
    return this.httpClient.get(url);
  }

  postMulta(concepto: string, importe: number): Observable<any> {
    let url = this.endpoint + 'ponmulta';
    console.trace('postMulta ' + url);
    let vehiculo: Vehiculo = this.vehiculoService.getVehiculoBuscado();
    let agenteJSON = this.autorizacionService.getAgente();
    let agente: Agente = new Agente(agenteJSON._nombre, agenteJSON._placa, "", agenteJSON._id);
    let multa: Multa = new Multa(vehiculo, agente, concepto, importe);
    let body = {
      "agente": { "id": multa.agente.id },
      "vehiculo": { "id": multa.vehiculo.id },
      "concepto": multa.concepto,
      "importe": multa.importe
    };
    return this.httpClient.post(url, body);
  }

  accionMulta(id: number, accion: string): Observable<any> {
    let url = this.endpoint + 'accionmulta/' + id + '/' + accion;
    console.trace('recuperarrMulta ' + url);
    let body = {
      "id": id
    };
    return this.httpClient.patch(url, body);
  }

}
