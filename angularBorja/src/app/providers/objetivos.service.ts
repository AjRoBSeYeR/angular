import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GLOBAL } from '../global';

@Injectable({
  providedIn: 'root'
})
export class ObjetivosService {
  endpoint :String;
  constructor(
    private httpClient: HttpClient
  ) {
    this.endpoint= GLOBAL.server + 'wsrestBorja/api/agente/estadisticas/';
  }

  totalAnual(){

  }

  leerObjetivos(id: number) {
    let url = this.endpoint + '/' + id;
    console.trace('recuperarrMulta ' + url);
    let body = {
      "id": id
    };
    return this.httpClient.patch(url, body);
  }
}
