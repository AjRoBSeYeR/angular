import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Vehiculo } from '../model/vehiculo';
import { GLOBAL } from '../global';

@Injectable({
  providedIn: 'root'
})
export class VehiculoService {
  private storage = window.sessionStorage;
  private vehiculoBuscado: Vehiculo;
  constructor(
    private httpClient: HttpClient
  ) {

  }

  obtenerVehiculoMatricula(matricula: string): Observable<any> {
    let url = GLOBAL.server + 'wsrestBorja/api/vehiculo/' + matricula;
    return this.httpClient.get(url);
  }

  pasaVehiculo(vehiculo: Vehiculo) {
    this.vehiculoBuscado = vehiculo;

  }
  getVehiculoBuscado(): Vehiculo {
    return this.vehiculoBuscado;
  }
}
