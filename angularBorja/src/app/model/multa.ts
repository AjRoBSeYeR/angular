import { Vehiculo } from './vehiculo';
import { Agente } from './agente';

export class Multa {

    private _id: number;
    private _fecha: string;
    private _vehiculo: Vehiculo;
    private _agente: Agente;
    private _concepto: string;
    private _importe: number;
    

    constructor(vehiculo: Vehiculo, agente: Agente, concepto: string, importe: number) {
        this._agente = agente;
        this._vehiculo = vehiculo;
        this._concepto = concepto;
        this._importe = importe;
    }
    public get importe(): number {
        return this._importe;
    }
    public set importe(value: number) {
        this._importe = value;
    }

    public get concepto(): string {
        return this._concepto;
    }
    public set concepto(value: string) {
        this._concepto = value;
    }


    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }

    public get fecha(): string {
        return this._fecha;
    }
    public set fecha(value: string) {
        this._fecha = value;
    }

    public get vehiculo(): Vehiculo {
        return this._vehiculo;
    }
    public set vehiculo(value: Vehiculo) {
        this._vehiculo = value;
    }

    public get agente(): Agente {
        return this._agente;
    }
    public set agente(value: Agente) {
        this._agente = value;
    }
}
