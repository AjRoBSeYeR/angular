package com.ipartek.formacion.service.impl;

import java.sql.SQLException;
import java.util.List;

import com.ipartek.formacion.modelo.daos.AgenteDAO;
import com.ipartek.formacion.modelo.daos.EstadisticasDAO;
import com.ipartek.formacion.modelo.daos.MultaDAO;
import com.ipartek.formacion.modelo.daos.VehiculoDAO;
import com.ipartek.formacion.modelo.pojo.Agente;
import com.ipartek.formacion.modelo.pojo.Multa;
import com.ipartek.formacion.modelo.pojo.Vehiculo;
import com.ipartek.formacion.service.AgenteService;

public class AgenteServiceImpl implements AgenteService {

	private static AgenteServiceImpl INSTANCE = null;
	private AgenteDAO agenteDAO;
	private MultaDAO multaDAO;
	private VehiculoDAO vehiculoDAO;
	private EstadisticasDAO estadisticasDAO;

	private AgenteServiceImpl() {
		super();
		agenteDAO = AgenteDAO.getInstance();
		vehiculoDAO = VehiculoDAO.getInstance();
		multaDAO = MultaDAO.getInstance();
	}

	public static synchronized AgenteServiceImpl getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new AgenteServiceImpl();
		}
		return INSTANCE;
	}

	@Override
	public Agente existe(String numeroPlaca, String password) {
		Agente agente = agenteDAO.login(numeroPlaca, password);
		return agente;
	}

	@Override
	public Multa multar(int idCoche, int idAgente, String concepto, float importe) throws Exception {
		Multa multa;
		if (concepto == null || importe < 1) {
			multa = null;
			throw new Exception();
		} else {
			Vehiculo vehiculo = vehiculoDAO.getById(idCoche);
			Agente agente = agenteDAO.getById(idAgente);
			multa = new Multa(importe, concepto, agente, vehiculo);
			multa = multaDAO.insert(multa);
		}
		return multa;
	}

	public List<Agente> getAll() {
		List<Agente> agentes = agenteDAO.getAll();
		return agentes;

	}

	public Boolean anularMulta(int id) throws SQLException {
		Boolean haSidoAnulada = false;
		Multa multa = multaDAO.getById(id, "alta");
		haSidoAnulada = multaDAO.update(multa, "baja");
		return haSidoAnulada;
	}

	public Boolean recuperarMulta(int id) throws SQLException {
		Boolean haSidoRecuperada = false;
		Multa multa = multaDAO.getById(id, "alta");
		haSidoRecuperada = multaDAO.update(multa, "recuperar");
		return haSidoRecuperada;
	}

	public Boolean accionMulta(int id, String accion) throws SQLException {
		Boolean accionCorrecta = false;
		Multa multa = multaDAO.getById(id, "alta");
		accionCorrecta = multaDAO.update(multa, accion);
		return accionCorrecta;
	}

	public Integer objetivoAnual() {
		Integer total=0;
		total= estadisticasDAO.getObjetivoAnual(2019);
		return total;
	}

	@Override
	public List<Multa> obtenerMultas(int idAgente, String cualVer) {
		List<Multa> multas = agenteDAO.getMultasByAgente(idAgente, cualVer);
		return multas;
	}

}
