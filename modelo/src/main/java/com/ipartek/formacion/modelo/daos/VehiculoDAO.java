package com.ipartek.formacion.modelo.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.ipartek.formacion.modelo.cm.ConnectionManager;
import com.ipartek.formacion.modelo.pojo.Vehiculo;

public class VehiculoDAO {
	private final static Logger LOG = Logger.getLogger(VehiculoDAO.class);
	private static VehiculoDAO INSTANCE = null;
	private static final String SQL_GETALL = "SELECT * FROM coche ORDER BY id DESC LIMIT 100";
	private static final String SQL_GETBYMATRICULA = "{call pa_coche_getByMatricula(?)}";
	private static final String SQL_GETBYID = "SELECT * FROM COCHE WHERE ID=?";

	private VehiculoDAO() {
		super();
	}

	public synchronized static VehiculoDAO getInstance() {

		if (INSTANCE == null) {
			INSTANCE = new VehiculoDAO();
		}
		return INSTANCE;
	}

	public ArrayList<Vehiculo> getAll() {

		ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
		try (Connection conn = ConnectionManager.getConnection();

				PreparedStatement pst = conn.prepareStatement(SQL_GETALL);
				ResultSet rs = pst.executeQuery()) {

			while (rs.next()) {
				try {
					Vehiculo vehiculo = new Vehiculo();
					vehiculo.setId(rs.getInt("id"));
					vehiculo.setMatricula(rs.getString("matricula"));
					vehiculo.setKm(rs.getInt("km"));
					vehiculo.setModelo(rs.getString("modelo"));
					vehiculos.add(vehiculo);
				} catch (Exception e) {
					LOG.error(e);
				}
				LOG.info("lista de vehiculos obtenida");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return vehiculos;
	}

	public Vehiculo getByMatricula(String matricula) {
		Vehiculo v = null;
		try (Connection conn = ConnectionManager.getConnection();
				CallableStatement cs = conn.prepareCall(SQL_GETBYMATRICULA);) {

			cs.setString(1, matricula);

			try (ResultSet rs = cs.executeQuery()) {
				try {
					while (rs.next()) {
						v = rowMapper(rs);
						LOG.info("vehiculo " + v.getMatricula() + " obtenido");
					}
				} catch (Exception e) {
					LOG.warn(e);
				}
			}

		} catch (Exception e) {
			LOG.error(e);
		}
		return v;
	}

	public Vehiculo getById(Integer id) {

		Vehiculo v = null;
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement cs = conn.prepareStatement(SQL_GETBYID);) {

			cs.setInt(1, id);

			try (ResultSet rs = cs.executeQuery()) {

				while (rs.next()) {
					v = rowMapper(rs);
					LOG.info("vehiculo " + v.getId() + " obtenido");
				}
			}

		} catch (Exception e) {
			LOG.error("El agente que buscas no existe", e);
		}
		return v;
	}

	private Vehiculo rowMapper(ResultSet rs) throws SQLException {
		Vehiculo v = new Vehiculo();
		v.setId(rs.getInt("id"));
		v.setMatricula(rs.getString("matricula"));
		v.setModelo(rs.getString("modelo"));
		v.setKm(rs.getInt("km"));
		return v;
	}
}
