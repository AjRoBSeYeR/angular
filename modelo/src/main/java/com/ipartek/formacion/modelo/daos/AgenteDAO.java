package com.ipartek.formacion.modelo.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.ipartek.formacion.modelo.cm.ConnectionManager;
import com.ipartek.formacion.modelo.pojo.Agente;
import com.ipartek.formacion.modelo.pojo.Multa;
import com.ipartek.formacion.modelo.pojo.Vehiculo;

public class AgenteDAO {

	private final static Logger LOG = Logger.getLogger(AgenteDAO.class);
	private static final String SQL_GETALL = "SELECT * FROM agente ORDER BY id DESC LIMIT 100";
	private static final String SQL_GETBYID = "SELECT * FROM agente WHERE id=?";
	private static final String SQL_AGENTELOGIN = "SELECT * FROM agente where placa=? and password= ?";
	private static final String SQL_GETMULTASBYAGENTE = "{call pa_multa_getByAgenteId(?, ?)}";
	private static AgenteDAO INSTANCE = null;

	private AgenteDAO() {
		super();
	}

	public synchronized static AgenteDAO getInstance() {

		if (INSTANCE == null) {
			INSTANCE = new AgenteDAO();
		}
		return INSTANCE;
	}

	public ArrayList<Agente> getAll() {

		ArrayList<Agente> agentes = new ArrayList<Agente>();
		try (Connection conn = ConnectionManager.getConnection();

				PreparedStatement pst = conn.prepareStatement(SQL_GETALL);
				ResultSet rs = pst.executeQuery()) {

			while (rs.next()) {
				try {
					Agente agente = rowMapperAgente(rs);
					agentes.add(agente);
				} catch (Exception e) {
					LOG.error(e);
				}
			}
			LOG.info("agentes obtenidos");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return agentes;
	}

	public Agente getById(Integer id) {

		Agente agente = null;
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement cs = conn.prepareStatement(SQL_GETBYID);) {

			cs.setInt(1, id);

			try (ResultSet rs = cs.executeQuery()) {

				while (rs.next()) {
					agente = rowMapperAgente(rs);
					LOG.info("magente obtenido");
				}
			}

		} catch (Exception e) {
			LOG.error("El agente que buscas no existe", e);
		}
		return agente;
	}

	public Agente login(String placa, String pass) {
		Agente agente = null;
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement cs = conn.prepareStatement(SQL_AGENTELOGIN);) {

			cs.setString(1, placa);
			cs.setString(2, pass);

			try (ResultSet rs = cs.executeQuery()) {

				while (rs.next()) {
					agente = rowMapperAgente(rs);
					LOG.info("agente " + agente.getNombre() + " logueado");
				}
			}

		} catch (Exception e) {
			LOG.error("El agente que buscas no existe", e);
		}
		return agente;
	}

	public ArrayList<Multa> getMultasByAgente(Integer id, String cualVer) {
		ArrayList<Multa> multas = new ArrayList<Multa>();
		Multa multa = null;
		try (Connection conn = ConnectionManager.getConnection();
				CallableStatement cs = conn.prepareCall(SQL_GETMULTASBYAGENTE);) {

			cs.setLong(1, id);
			cs.setString(2, cualVer);
			Agente agente = getById(id);
			try (ResultSet rs = cs.executeQuery()) {

				while (rs.next()) {
					multa = rowMapperMulta(rs, agente);
					multas.add(multa);
				}
				LOG.info("obtenidas multas del agente");
			}

		} catch (Exception e) {
			LOG.error("El agente que buscas no existe", e);
		}
		return multas;
	}

	public ArrayList<Multa> getMultasAnuladasByAgente(Integer id) {
		ArrayList<Multa> multas = new ArrayList<Multa>();
		Multa multa = null;
		try (Connection conn = ConnectionManager.getConnection();
				CallableStatement cs = conn.prepareCall(SQL_GETMULTASBYAGENTE);) {

			cs.setLong(1, id);
			cs.setString(2, "baja");
			Agente agente = getById(id);
			try (ResultSet rs = cs.executeQuery()) {

				while (rs.next()) {
					multa = rowMapperMulta(rs, agente);
					multas.add(multa);
				}
				LOG.info("obtenidas multas anuladas del agente");
			}

		} catch (Exception e) {
			LOG.error("El agente que buscas no existe", e);
		}
		return multas;
	}

	private Agente rowMapperAgente(ResultSet rs) throws SQLException {
		Agente a = new Agente();
		a.setId(rs.getInt("id"));
		a.setNombre(rs.getString("nombre"));
		a.setPlaca(rs.getString("placa"));
		a.setPassword(rs.getString("password"));
		return a;
	}

	private Multa rowMapperMulta(ResultSet rs, Agente agente) throws SQLException {
		Multa m = new Multa();
		m.setId(rs.getLong("id_multa"));
		m.setImporte(rs.getFloat("importe"));
		m.setConcepto(rs.getString("concepto"));
		m.setFechaAlta(rs.getDate("fecha_alta"));
		Vehiculo v = new Vehiculo();
		v.setId(rs.getInt("id_coche"));
		v.setMatricula(rs.getString("matricula"));
		v.setModelo(rs.getString("modelo"));
		v.setKm(rs.getInt("km"));
		m.setVehiculo(v);
		m.setAgente(agente);
		return m;
	}

}
